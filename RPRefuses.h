//
//  RPRefuses.h
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPRefuses : NSObject
@property (strong, nonatomic) NSArray *refuses;
+ (RPRefuses*) sharedRefuses;
@end
