//
//  RPLocalAddress.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/12/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@interface RPLocalAddress : UIViewController
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *house;
@property (nonatomic, strong) NSString *korpus;
@property (nonatomic, strong) NSString *stroenie;
@property (nonatomic, assign) CLLocationCoordinate2D location;

@property (nonatomic, strong) NSString *cityTo;
@property (nonatomic, strong) NSString *streetTo;
@property (nonatomic, strong) NSString *houseTo;
@property (nonatomic, strong) NSString *korpusTo;
@property (nonatomic, strong) NSString *stroenieTo;
@property (nonatomic, assign) CLLocationCoordinate2D locationTo;

@property (nonatomic, strong) NSString *dateAdd;
@property (nonatomic, assign) NSInteger addressId;
@property (nonatomic, assign) NSInteger orderId;
@end
