//
//  RPTarif.h
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPTarif : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSInteger summFirstHour;
@property (nonatomic, assign) NSInteger summNextMinute;
@property (nonatomic, assign) NSInteger summNextKm;
@property (nonatomic, assign) NSInteger time;
@property (nonatomic, assign) NSInteger distanceFromMkad;
@property (nonatomic, assign) NSInteger summForTime;
@property (nonatomic, assign) NSInteger summForDistance;
@property (nonatomic, assign) NSInteger summForItog;
@property (nonatomic, assign) NSInteger summForTarif;
@property (nonatomic, assign) NSInteger summForBal;
@property (nonatomic, assign) NSInteger summPayBal;
@property (nonatomic, assign) NSInteger addBal;
@end
