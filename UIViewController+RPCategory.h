//
//  UIViewController+RPCategory.h
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/18/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (RPCategory)
-(void)showErrorMessage:(NSString*)errorMessage;
-(void)showWarningMessage:(NSString*)errorMessage;
-(void)showSuccessMessage:(NSString*)message;
-(void)addLogoToNavigationBar;
@end
