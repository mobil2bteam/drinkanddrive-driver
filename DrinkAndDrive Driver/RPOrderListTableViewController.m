//
//  RPOrderListTableViewController.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderListTableViewController.h"
#import "RPOrderListCell.h"
#import "Header.h"
#import "RPOrderDescriptionController.h"
@interface RPOrderListTableViewController()<MBProgressHUDDelegate>
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) NSArray <RPOrder *> *ordersArray;
@end

@implementation RPOrderListTableViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self loadOrders];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if (indexPath) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

#pragma mark - UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.ordersArray.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RPOrderListCell *cell = (RPOrderListCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    RPOrder *order = self.ordersArray[indexPath.row];
    cell.numberOrderLabel.text = [NSString stringWithFormat:@"№%ld",(long)order.numberOrder];
    cell.addressALabel.text = order.adddressB;
    cell.addressBLabel.text = order.adddressA;
    cell.dateLabel.text = order.date;
    cell.summLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)order.itog];
    for (UIImageView *star in cell.starImageViews) {
        [star setImage:[UIImage imageNamed:@"StarActive"]];
    }
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}



#pragma mark - Methods
-(void)hideIndicatorView{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}
-(void)addHudToView{
    self.HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.HUD];
    self.HUD.dimBackground = NO;
    self.HUD.delegate = self;
    self.HUD.labelText = @"Загрузка...";
}

-(void)loadOrders{
    NSDictionary *dic = @{@"type":@"orderlist",
                          @"password":[RPUser sharedUser].password,
                          @"driverid":@([RPUser sharedUser].driverid)
                          };
    
    [self addHudToView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.HUD showAnimated:YES whileExecutingBlock:^{
        [[RPServerManager sharedManager] getClosedOrdersList:dic onSuccess:^(NSMutableArray<RPOrder *> *orderList, NSString *errorMessage) {
            [self hideIndicatorView];
            if (errorMessage==nil) {
                self.ordersArray = [orderList copy];
                [self.tableView reloadData];
            }
            else {
                [self showErrorMessage:errorMessage];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    }];
    
}

#pragma mark - Navigation
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    return YES;
    //    return [identifier isEqualToString:@"orderSegue"];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"detailSegue"]) {
        RPTabBarController *destinationController = [segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        RPOrder *selectedOrder = self.ordersArray[indexPath.row];
        destinationController.orderToken = selectedOrder.token;
        destinationController.isClosedOrder=YES;
    }
}

@end
