//
//  RPRootViewController.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/9/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPRootViewController.h"
#import "RPDriverDesktopViewController.h"
#import "Header.h"
#import "AppDelegate.h"

@interface RPRootViewController()<UITextFieldDelegate,MBProgressHUDDelegate>
@property (nonatomic) AppDelegate* appDelegate;
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField1;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField2;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField3;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField4;
@property (nonatomic, strong) NSString *phone;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *passwordTextField;
@property (strong, nonatomic) NSUserDefaults *userDefaults;

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@end
@implementation RPRootViewController

#pragma mark - Lifecycle
-(void)viewDidLoad{
    [super viewDidLoad];
    self.appDelegate = [[UIApplication sharedApplication]delegate];
    for (UIView *view in @[self.phoneView, self.passwordView]) {
        view.layer.borderColor = [UIColor colorWithRed:0.6 green:1.0 blue:0.4 alpha:0.7978178879].CGColor;
        view.layer.borderWidth = 1.0f;
    }
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    [self loadData];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.backgroundView.hidden=YES;
    self.phoneTextField1.text = self.phoneTextField2.text = self.phoneTextField3.text = self.phoneTextField4.text= self.passwordTextField.text = @"";
}

#pragma mark - Properties

- (NSString*)phone{
    NSString *phone = [NSString stringWithFormat:@"%@%@%@%@", self.phoneTextField1.text, self.phoneTextField2.text, self.phoneTextField3.text, self.phoneTextField4.text];
    return phone;
}

#pragma mark - UITextField Delegate

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(JJMaterialTextfield *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2 ||
        textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if ([string isEqualToString:@""]) {
            if (textField.text.length == 1) {
                textField.text = @"";
                if (textField == self.phoneTextField4) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                if (textField == self.phoneTextField3) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField1 becomeFirstResponder];
                }
                return NO;
            }
            return YES;
        }
        NSScanner *scanner = [NSScanner scannerWithString:string];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        if (!isNumeric) {
            return NO;
        }
    }
    
    if (textField == self.phoneTextField3 ||
        textField == self.phoneTextField4) {
        if (textField.text.length > 0) {
            if (textField.text.length == 1) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField == self.phoneTextField3) {
                [self.phoneTextField4 becomeFirstResponder];
            }
            if (textField == self.phoneTextField4) {
                [self.passwordTextField becomeFirstResponder];
            }
            [textField resignFirstResponder];
            return NO;
        }
        return YES;
    }
    
    if (textField == self.phoneTextField1 ||
        textField == self.phoneTextField2) {
        if (textField.text.length > 0) {
            if (textField.text.length < 3) {
                textField.text = [textField.text stringByAppendingString:string];
            }
            if (textField.text.length == 3) {
                if (textField == self.phoneTextField1) {
                    [self.phoneTextField2 becomeFirstResponder];
                }
                if (textField == self.phoneTextField2) {
                    [self.phoneTextField3 becomeFirstResponder];
                }
                [textField resignFirstResponder];
            }
            return NO;
        }
        return YES;
        
    }
    if (string.length!=0)
        [textField hideError];
    if (textField.text.length==1 && [string isEqualToString:@""]) {
        [textField showError];
    }
    return YES;
}

#pragma mark - Actions
- (IBAction)enterButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (self.passwordTextField.text.length==0 || self.phone.length != 10){
        if (self.passwordTextField.text.length==0) {
            [self.passwordTextField showError];
        }
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        return;
    }
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setObject:self.phone forKey:@"phone"];
    [userDef synchronize];

    NSDictionary *dic = @{@"type":@"login",
                          @"phone": self.phone,
                          @"password":[self.passwordTextField.text md5]
                          };
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                [[RPServerManager sharedManager] getConfigsOnSuccess:^(RPOptions *configs) {
            [[RPServerManager sharedManager] loginWithParameters:dic onSuccess:^(RPUser *user, NSString *errorMessage) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                if (errorMessage==nil) {
                    [self.appDelegate sendTokenToServer];
                    [self performSegueWithIdentifier:@"loginSegue" sender:self];
                }
                else {
                    self.backgroundView.hidden=YES;
                    [self showErrorMessage:errorMessage];
                }
                
            } onFailure:^(NSError *error, NSInteger statusCode) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                self.backgroundView.hidden=YES;
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                [self showErrorMessage:@"Произошла ошибка!"];
            }];
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            self.backgroundView.hidden=YES;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showErrorMessage:@"Произошла ошибка!"];
        }];

    });

}

#pragma mark  Navigation
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    return  [identifier isEqualToString:@"loginSegue"];
}

-(void)loadData{
        if ([self.userDefaults valueForKey:@"phone"]!=nil && [self.userDefaults valueForKey:@"password"]) {
        NSDictionary *dic = @{@"type":@"login",
                              @"phone": [self.userDefaults valueForKey:@"phone"],
                              @"password": [self.userDefaults valueForKey:@"password"]
                              };
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Загрузка";
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [[RPServerManager sharedManager] getConfigsOnSuccess:^(RPOptions *configs) {
                
                [[RPServerManager sharedManager] loginWithParameters:dic onSuccess:^(RPUser *user, NSString *errorMessage) {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    if (errorMessage==nil) {
                        [self.appDelegate sendTokenToServer];
                        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"desktopNav"] animated:NO completion:^{
                            self.backgroundView.hidden=YES;
                        }];
                    }
                    else {
                        [self.userDefaults removeObjectForKey:@"phone"];
                        [self.userDefaults removeObjectForKey:@"password"];
                        [self.userDefaults synchronize];
                        self.backgroundView.hidden=YES;
                        [self showErrorMessage:errorMessage];
                    }
                    
                } onFailure:^(NSError *error, NSInteger statusCode) {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                    self.backgroundView.hidden=YES;
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    [self showErrorMessage:@"Произошла ошибка!"];
                }];
                
                
            } onFailure:^(NSError *error, NSInteger statusCode) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                self.backgroundView.hidden=YES;
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                [self showErrorMessage:@"Произошла ошибка!"];
            }];
        });
        
    } else {
        if ([self.userDefaults valueForKey:@"phone"]!=nil) {
            NSString *phone = [self.userDefaults stringForKey:@"phone"];
            self.phoneTextField1.text = [phone substringWithRange:NSMakeRange(0, 3)];
            self.phoneTextField2.text = [phone substringWithRange:NSMakeRange(3, 3)];
            self.phoneTextField3.text = [phone substringWithRange:NSMakeRange(6, 2)];
            self.phoneTextField4.text = [phone substringWithRange:NSMakeRange(8, 2)];
        }
        self.backgroundView.hidden=YES;
    }
}

@end
