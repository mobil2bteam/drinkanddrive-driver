//
//  RPOrderCell.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCurrentOrderCell.h"

@implementation RPCurrentOrderCell
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:YES];
    if (!highlighted) {
        self.contentView.backgroundColor = [UIColor colorWithRed:0.9804 green:0.6902 blue:0.1843 alpha:1.0];
    } else
    {
        self.contentView.backgroundColor = [UIColor colorWithRed:0.6 green:1.0 blue:0.4 alpha:1.0];
    }
}

@end
