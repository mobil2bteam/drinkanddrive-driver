//
//  RPCloseOrderController.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCloseOrderController.h"
#import "Header.h"

@interface RPCloseOrderController()<MBProgressHUDDelegate>
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) RPTarif *tarif;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstHourLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextMinuteLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextKmLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *summTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumKmLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumTarifLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumPayBalLabel;
@property (weak, nonatomic) IBOutlet UILabel *itogLabel;
@property (weak, nonatomic) IBOutlet UILabel *addBalLabel;
@end

@implementation RPCloseOrderController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self loadTarif];
}

- (IBAction)closeOrderButton:(id)sender {
    NSDictionary *dic = @{@"type":@"setstatus",
                          @"password":[RPUser sharedUser].password,
                          @"driverid":@([RPUser sharedUser].driverid),
                          @"token":self.token,
                          @"full_info":@(1),
                          @"status":@(6),
                          @"lan":@(self.userLocation.latitude),
                          @"lng":@(self.userLocation.longitude)
                          };

    [self addHudToView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.HUD showAnimated:YES whileExecutingBlock:^{
        [[RPServerManager sharedManager] setStatus:dic onSuccess:^(NSString *errorMessage) {
            [self hideIndicatorView];
            if (errorMessage==nil) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            else {
                [self showErrorMessage:errorMessage];
            }

        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    }];
}
- (IBAction)cancelButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Methods
-(void)hideIndicatorView{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}
-(void)addHudToView{
    self.HUD = [[MBProgressHUD alloc] initWithView:self.scrollView];
    [self.scrollView addSubview:self.HUD];
    self.HUD.dimBackground = NO;
    self.HUD.delegate = self;
    self.HUD.labelText = @"Загрузка...";
}
-(void)loadTarif{
    
    NSDictionary *dic1 = @{@"type":@"mycoordinates",
                           @"password":[RPUser sharedUser].password,
                           @"driverid":@([RPUser sharedUser].driverid),
                           @"lan":@(self.userLocation.latitude),
                           @"lng":@(self.userLocation.longitude)
                           };
    
    
    
    NSDictionary *dic = @{@"type":@"check_order",
                          @"password":[RPUser sharedUser].password,
                          @"driverid":@([RPUser sharedUser].driverid),
                          @"token":self.token,
                          };
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Загрузка";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
    [[RPServerManager sharedManager]pushCoordinate:dic1
                                             onSuccess:^(NSString *errorMessage) {
     if (errorMessage == nil) {
                 [[RPServerManager sharedManager] getTarif:dic onSuccess:^(RPTarif *tarif, NSString *errorMessage) {
                     if (errorMessage==nil) {
                         self.tarif = tarif;
                         [self prepareView];
                     }
                     else {
                         [self showErrorMessage:errorMessage];
                     }
                 } onFailure:^(NSError *error, NSInteger statusCode) {
                     [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                     [self showErrorMessage:@"Произошла ошибка"];
                 }];
             } else  {
                 [self hideIndicatorView];
                 [self showErrorMessage:errorMessage];
             }
     
     } onFailure:^(NSError *error, NSInteger statusCode) {
         [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [self showErrorMessage:@"Произошла ошибка"];
     }];
    });
}

-(void)prepareView{
    self.nameLabel.text = self.tarif.name;
    self.firstHourLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.tarif.summFirstHour];
    self.nextMinuteLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.tarif.summNextMinute];
    self.nextKmLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.tarif.summNextKm];
    NSInteger hour = self.tarif.time/60;
    NSInteger minute = self.tarif.time%60;
    self.timeLabel.text = [NSString stringWithFormat:@"%ld ч. %ld м.", (long)hour, (long)minute];
    CGFloat km = self.tarif.distanceFromMkad/1000.0;
    self.distanceLabel.text = [NSString stringWithFormat:@"%.3f км.",km];
    
    self.summTimeLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.tarif.summForTime];
    self.sumKmLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.tarif.summForDistance];
    self.sumTarifLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.tarif.summForTarif];
    self.sumPayBalLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.tarif.summPayBal];
    self.itogLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.tarif.summForItog];
    self.addBalLabel.text =  [NSString stringWithFormat:@"+%ld", (long)self.tarif.addBal];
    self.scrollView.scrollEnabled=YES;
    self.containerView.hidden=NO;
    [self hideIndicatorView];
}
@end
