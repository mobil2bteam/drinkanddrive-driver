//
//  RPCloseOrderController.h
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreLocation;
@interface RPCloseOrderController : UIViewController
@property (strong, nonatomic) NSString *token;
@property (assign, nonatomic) CLLocationCoordinate2D userLocation;
@end
