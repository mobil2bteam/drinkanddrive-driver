//
//  RPOrderDescriptionController.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderDescriptionController.h"
#import "Header.h"
#import "RPRefuseViewController.h"
#import "RPRouteViewController.h"
#import "AppDelegate.h"
#define klocation  self.appDelegate.locationManager.location.coordinate

@interface RPOrderDescriptionController()<MBProgressHUDDelegate>
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (weak, nonatomic) IBOutlet UIButton *checkStatusButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

//главный блок
@property (weak, nonatomic) IBOutlet UILabel *numberOrderLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateCreateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateStartLabel;
@property (weak, nonatomic) IBOutlet UILabel *datePlaneLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateFinishLabel;
@property (weak, nonatomic) IBOutlet UIView *topView;

//расчеты
@property (weak, nonatomic) IBOutlet UIView *payView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *summLabel;
@property (weak, nonatomic) IBOutlet UILabel *summBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *addBallLabel;
@property (weak, nonatomic) IBOutlet UILabel *itogLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payHeightConstraint;

//автомобиль
@property (weak, nonatomic) IBOutlet UIView *carView;
@property (weak, nonatomic) IBOutlet UILabel *markaLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberCarLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *carHeightConstraint;

//комментарий
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentViewHeightConstraint;

//оценка
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *ocenkaImageViewArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ocenkaHeightConstraint;

//вью с информацией о клиенте
@property (weak, nonatomic) IBOutlet UIView *clientView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *clientViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *clientNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *clientPhoneButton;

@property (weak, nonatomic) IBOutlet UIView *checkStatusView;
@property (weak, nonatomic) IBOutlet UIView *refuseView;
@property (weak, nonatomic) IBOutlet UIView *routeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *routeViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *refuseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *checkStatusViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *currentTimeVeiwHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet UIView *currentTimeView;
@property (weak, nonatomic) IBOutlet UIView *ocenkaView;

@end

@implementation RPOrderDescriptionController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.appDelegate = [[UIApplication sharedApplication]delegate];
    self.token = ((RPTabBarController*)self.tabBarController).orderToken;
    self.checkStatusView.hidden=YES;
    self.checkStatusViewHeightConstraint.priority=999;
    self.refuseView.hidden=YES;
    self.refuseViewHeightConstraint.priority=999;
    self.routeView.hidden=YES;
    self.routeViewHeightConstraint.priority=999;
    [self.scrollView layoutIfNeeded];
    self.scrollView.hidden=YES;
    [self loadOrders];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
#pragma mark - Methods
-(void)loadOrders{
        NSDictionary *dic = @{@"type":@"get",
                              @"password":[RPUser sharedUser].password,
                              @"userid":@([RPUser sharedUser].driverid),
                              @"token":self.token,
                              @"full_info":@(1),
                              @"driver":@(1)
                              };
    
    [self addHudToView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.HUD showAnimated:YES whileExecutingBlock:^{
        [[RPServerManager sharedManager] getOrderFull:dic onSuccess:^(RPOrderFull *order, NSString *errorMessage) {
            if (errorMessage==nil) {
                self.order=order;
                [self fillAllControls];
            }
            else {
                [self hideIndicatorView];
                [self showErrorMessage:errorMessage];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    }];
    
}

-(void)hideIndicatorView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}

-(void)addHudToView{
    self.HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.HUD];
    self.HUD.dimBackground = NO;
    self.HUD.delegate = self;
    self.HUD.labelText = @"Загрузка...";
}

-(void)fillCheckStatusButtonText{
    switch (self.order.status) {
        case 1:
            [self.checkStatusButton setTitle:@"БЕРУ ЗАЯВКУ" forState:UIControlStateNormal];
            break;
        case 2:
            [self.checkStatusButton setTitle:@"ВЫХОЖУ" forState:UIControlStateNormal];
            break;
        case 3:
            [self.checkStatusButton setTitle:@"Я НА МЕСТЕ" forState:UIControlStateNormal];
            break;
        case 4:
        case 10:
        case 11:
            [self.checkStatusButton setTitle:@"ПОЕХАЛИ" forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}
- (IBAction)checkStatusButtonPressed:(id)sender {
    if ([RPUser sharedUser].status == 1) {
        [self showErrorMessage:@"Вы не можете брать заявки"];
        return;
    }
    //переключаем статус заявки на статус 5
    NSInteger status;
    if (self.order.status < 5) {
        status = self.order.status+1;
    } else status = 5;
    NSDictionary *dic = @{@"type":@"setstatus",
                          @"password":[RPUser sharedUser].password,
                          @"driverid":@([RPUser sharedUser].driverid),
                          @"token":self.token,
                          @"full_info":@(1),
                          @"status":@(status),
                          @"lan":@(klocation.latitude),
                          @"lng":@(klocation.longitude)
                          };
    [self addHudToView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.HUD showAnimated:YES whileExecutingBlock:^{
        
        [[RPServerManager sharedManager]setStatus:dic onSuccess:^(NSString *errorMessage) {
            if (errorMessage==nil) {
                NSDictionary *param = @{@"type":@"get",
                                        @"password":[RPUser sharedUser].password,
                                        @"userid":@([RPUser sharedUser].driverid),
                                        @"token":self.token,
                                        @"full_info":@(1),
                                        @"driver":@(1)
                                        };
                
                [[RPServerManager sharedManager] getOrderFull:param onSuccess:^(RPOrderFull *order, NSString *errorMessage) {
                    if (errorMessage==nil) {
                        self.order=order;
                        //если статус 3, то начинаем отслеживать координаты
                        if (order.status==3) {
//                            [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"pushCoordinate"];
//                            [[NSUserDefaults standardUserDefaults]synchronize];
                        }
                        //если статус 5, то переходим на режим "Маршрут"
                        if (order.status == 5) {
                            [self performSegueWithIdentifier:@"routeSegue" sender:self];
                        }
                        [self fillAllControls];
                    }
                    else {
                        [self hideIndicatorView];
                        [self showErrorMessage:errorMessage];
                    }
                } onFailure:^(NSError *error, NSInteger statusCode) {
                    [self hideIndicatorView];
                    [self showErrorMessage:@"Произошла ошибка"];
                }];
            }
            else {
                [self hideIndicatorView];
                [self showErrorMessage:errorMessage];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
        
    }];
    
}

- (IBAction)clientPhoneButtonPressed:(id)sender {
    NSArray* words = [self.order.userPhone componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phone = [words componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phone]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

-(void)fillAllControls{
    self.numberOrderLabel.text = [NSString stringWithFormat:@"№%ld", (long)self.order.numberOrder];
    self.dateStartLabel.text = self.order.dateStart;
    self.datePlaneLabel.text= self.order.datePlane;
    self.dateFinishLabel.text = self.order.dateFinish;
    self.dateCreateLabel.text = self.order.dateCreate;
    self.currentTimeLabel.text = self.order.currentTime;
    self.clientNameLabel.text = self.order.userName;
    if ([self.order.wayList firstObject].location.latitude !=0) {
        [[LMGeocoder sharedInstance]reverseGeocodeCoordinate:self.order.wayList[0].location service:kLMGeocoderGoogleService completionHandler:^(NSArray *results, NSError *error) {
            if (results.count && !error) {
                LMAddress *address = [results firstObject];
                if (![NSString isEmpty:[address parseThoroughfare]]) {
                    self.addressFromLabel.text = [address parseThoroughfare];
                } else self.addressFromLabel.text=@",";
                
                if ([self.order.wayList lastObject].locationTo.latitude !=0) {
                    [[LMGeocoder sharedInstance]reverseGeocodeCoordinate:[self.order.wayList lastObject].locationTo service:kLMGeocoderGoogleService completionHandler:^(NSArray *results, NSError *error) {
                        
                        if (results.count && !error) {
                            LMAddress *address = [results firstObject];
                            if (![NSString isEmpty:[address parseThoroughfare]]) {
                                self.addressToLabel.text = [address parseThoroughfare];
                            } else self.addressToLabel.text=@",";
                        }
                    }];
                }
            }
        }];
    }
    self.timeLabel.text = self.order.time;
    self.summLabel.text = [NSString stringWithFormat:@"%ld руб", (long)self.order.summ];
    self.summBallLabel.text = [NSString stringWithFormat:@"%ld руб", (long)self.order.summBall];
    self.addBallLabel.text = [NSString stringWithFormat:@"%ld", (long)self.order.addBall];
    self.itogLabel.text = [NSString stringWithFormat:@"%ld руб", (long)self.order.itog];
    self.numberCarLabel.text = self.order.numberCar;
    self.markaLabel.text = self.order.marka;
    self.commentLabel.text = self.order.comment;
    [self fillCheckStatusButtonText];
    [self prepareView];
    [self hideIndicatorView];
}

-(void)prepareView{
    
        if (self.order.currentTime.length==0) {
            // удаляем время в пути
            self.currentTimeView.hidden=YES;
            self.currentTimeVeiwHeightConstraint.priority=999;
        } else {
            self.currentTimeView.hidden=NO;
            self.currentTimeVeiwHeightConstraint.priority=250;
        }
        if (self.order.summ==0){ // удаляем расчеты
            self.payView.hidden=YES;
            self.payHeightConstraint.priority=999;
        } else {
                self.payView.hidden=NO;
                self.payHeightConstraint.priority=250;
                    if (self.order.time.length==0) {
                        self.timeView.hidden=YES;
                        self.timeViewHeightConstraint.priority=999;
                    } else {
                        self.payView.hidden=NO;
                        self.payHeightConstraint.priority=250;
                    }
            }
    
        if (self.order.marka.length==0){ //удаляем автоюобиль
            self.carView.hidden=YES;
            self.carHeightConstraint.priority=999;
        } else {
            self.carView.hidden=NO;
            self.carHeightConstraint.priority=250;
        }
    
        if (self.order.comment.length==0){ //удаляем comment
            self.commentView.hidden=YES;
            self.commentViewHeightConstraint.priority=999;
        } else {
            self.commentView.hidden=NO;
            self.commentViewHeightConstraint.priority=250;
        }
        if (self.order.ocenka==0){ //удаляем оценку
            self.ocenkaView.hidden=YES;
            self.ocenkaHeightConstraint.priority=999;
        } else {
            self.ocenkaView.hidden=NO;
            self.ocenkaHeightConstraint.priority=250;
            for (NSInteger i=0; i<self.order.ocenka; i++) {
                UIImageView *imageView = self.ocenkaImageViewArray[i];
                imageView.image = [UIImage imageNamed:@"StarActive"];
            }
        }
    if (!((RPTabBarController*)self.tabBarController).isClosedOrder) {
        if (self.order.status>=5 && self.order.status!=10 && self.order.status !=11){
            self.checkStatusView.hidden=YES;
            self.checkStatusViewHeightConstraint.priority=999;
        } else {
            self.checkStatusView.hidden=NO;
            self.checkStatusViewHeightConstraint.priority=250;
        }
        if ((self.order.status>0 && self.order.status<5) || (self.order.status==10 || self.order.status==11)){
            self.routeView.hidden=YES;
            self.routeViewHeightConstraint.priority=999;
        } else {
            self.routeView.hidden=NO;
            self.routeViewHeightConstraint.priority=250;
        }
        if (self.order.status>1) {
            self.refuseView.hidden=YES;
            self.refuseViewHeightConstraint.priority=999;
        } else {
            self.refuseView.hidden=NO;
            self.refuseViewHeightConstraint.priority=250;
        }
        // если есть информация о клиенте, то показываем ее
        if (((self.order.status >=2 && self.order.status<=5) || self.order.status == 10 || self.order.status == 11) && self.order.userPhone) {
            self.clientView.hidden=NO;
            self.clientViewHeightConstraint.priority=250;
        } else {
            self.clientView.hidden=YES;
            self.clientViewHeightConstraint.priority=999;
        }
    }
    [self.scrollView layoutIfNeeded];
    self.scrollView.hidden=NO;
}

#pragma mark - Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"refuseSegue"]) {
        RPRefuseViewController *destinationController = segue.destinationViewController;
        destinationController.token = ((RPTabBarController*)self.tabBarController).orderToken;
    }
    
    if ([segue.identifier isEqualToString:@"routeSegue"]) {
        RPRouteViewController *destinationController = segue.destinationViewController;
        destinationController.order = self.order;
    }

}

@end
