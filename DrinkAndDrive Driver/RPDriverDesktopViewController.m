//
//  RPDriverDesktopViewController.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPDriverDesktopViewController.h"
#import "RPCurrentOrderCell.h"
#import "RPNewOrderCell.h"
#import "RPOrderCell.h"
#import "Header.h"
#import "AppDelegate.h"

@interface RPDriverDesktopViewController()<UITableViewDelegate, UITableViewDataSource,MBProgressHUDDelegate, RPUpdateLocation>
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *driverStarsImageViews;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lastnameLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstnameLabel;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (strong, nonatomic) NSArray *ordersArray;
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@end

@implementation RPDriverDesktopViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.appDelegate = [[UIApplication sharedApplication]delegate];
    self.appDelegate.delegate=self;
    [self addLogoToNavigationBar];
    [self fillDriverInfo];
    [self addRefreshControll];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadOrders];
}

- (void)addRefreshControll{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(updateStatus) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    self.tableView.alwaysBounceVertical = YES;
}

-(void)updateStatus{
    [self loadOrders];
}

-(void)updateLocationWith:(CLLocationCoordinate2D)coordinate{
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    NSIndexPath *selectedRow = [self.tableView indexPathForSelectedRow];
    if (selectedRow) {
        [self.tableView deselectRowAtIndexPath:selectedRow animated:NO];
    }
}

-(void)setDriverOcenka:(NSInteger)ocenka{
    for (NSInteger i=0;i<ocenka; i++) {
        UIImageView *imageView = self.driverStarsImageViews[i];
        imageView.image = [UIImage imageNamed:@"StarActive"];
    }
    for (NSInteger i=ocenka;i<self.driverStarsImageViews.count; i++) {
        UIImageView *imageView = self.driverStarsImageViews[i];
        imageView.image = [UIImage imageNamed:@"StarNonActive"];
    }
}

-(void)fillDriverInfo{
    [self setDriverOcenka:[RPUser sharedUser].rating];
    self.lastnameLabel.text = [RPUser sharedUser].name1;
    self.firstnameLabel.text = [NSString stringWithFormat:@"%@ %@", [RPUser sharedUser].name2,[RPUser sharedUser].name3];
    self.driverImageView.image = nil;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",[RPOptions sharedOptions].url_suite,[RPUser sharedUser].foto]];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.driverImageView setImageWithURLRequest:imageRequest
                                placeholderImage:nil
                                         success:nil
                                         failure:nil];
        if ([RPUser sharedUser].status==3) {
        self.statusButton.enabled=NO;
        self.statusButton.backgroundColor = [UIColor colorWithRed:0.9804 green:0.6902 blue:0.1843 alpha:1.0];
    } else if ([RPUser sharedUser].status==2){
        self.statusButton.enabled=YES;
        self.statusButton.backgroundColor = [UIColor colorWithRed:0.6 green:1.0 blue:0.4 alpha:1.0];
    } else {
        self.statusButton.enabled=YES;
        self.statusButton.backgroundColor = [UIColor colorWithRed:0.5216 green:0.5216 blue:0.5216 alpha:1.0];
    }
    [self.statusButton setTitle:[RPUser sharedUser].statusText forState:UIControlStateNormal];
}

#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.ordersArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RPOrder *currentOrder = self.ordersArray[indexPath.row];
    
    if (currentOrder.isCurrent==YES) {
        // срочная заявка
        RPCurrentOrderCell *cell = (RPCurrentOrderCell*)[tableView dequeueReusableCellWithIdentifier:@"currentOrderCell" forIndexPath:indexPath];
        cell.addressALabel.text = currentOrder.adddressA;
        cell.addressBLabel.text = currentOrder.adddressB;
        cell.dateOrderLabel.text = currentOrder.date;
        cell.orderStatusLabel.text = currentOrder.statusText;
        cell.numberOrderLabel.text = [NSString stringWithFormat:@"№%ld",(long)currentOrder.numberOrder];
        cell.currentOrderLabel.text = @"ТЕКУЩАЯ ЗАЯВКА";
        if (currentOrder.flagTime==0) {
            cell.hotOrderImageViewWidth.constant=0;
        }
        [cell layoutIfNeeded];
        return cell;
        
    } else if (currentOrder.status==1){
        // новая заявка
        RPNewOrderCell *cell = (RPNewOrderCell*)[tableView dequeueReusableCellWithIdentifier:@"newOrderCell" forIndexPath:indexPath];
        cell.addressALabel.text = currentOrder.adddressA;
        cell.addressBLabel.text = currentOrder.adddressB;
        cell.dateOrderLabel.text = currentOrder.date;
        cell.orderStatusLabel.text = currentOrder.statusText;
        if (currentOrder.flagTime==0) {
            cell.hotOrderImageViewWidthConstraint.constant=0;
        }
        return cell;

    } else {
        RPOrderCell *cell = (RPOrderCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.addressALabel.text = currentOrder.adddressA;
        cell.addressBLabel.text = currentOrder.adddressB;
        cell.dateOrderLabel.text = currentOrder.date;
        cell.orderStatusLabel.text = currentOrder.statusText;
        if (currentOrder.flagTime==0) {
            cell.hotOrderImageViewWidth.constant=0;
        }
        [cell layoutIfNeeded];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 115;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"orderSegue" sender:self];
}

#pragma mark - Actions
- (IBAction)changeStatus:(id)sender {
    NSInteger toStatus = [RPUser sharedUser].status==1?2:1;
    NSDictionary *dic = @{@"type":@"setstatus",
                          @"idstatus": @(toStatus),
                          @"password":[RPUser sharedUser].password,
                          @"driverid":@([RPUser sharedUser].driverid)
                          };
    [self addHudToView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.HUD showAnimated:YES whileExecutingBlock:^{
        [[RPServerManager sharedManager] loginWithParameters:dic onSuccess:^(RPUser *user, NSString *errorMessage) {
            [self hideIndicatorView];
            if (errorMessage==nil) {
                [self fillDriverInfo];
            }
            else {
                [self showErrorMessage:errorMessage];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    }];

}

#pragma mark - Methods
-(void)hideIndicatorView{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}
-(void)addHudToView{
    self.HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.HUD];
    self.HUD.dimBackground = NO;
    self.HUD.delegate = self;
    self.HUD.labelText = @"Загрузка...";
}


-(void)loadOrders{
    self.ordersArray=nil;
    [self.tableView reloadData];
    NSDictionary *dic = @{@"type":@"getorder",
                          @"password":[RPUser sharedUser].password,
                          @"driverid":@([RPUser sharedUser].driverid)
                          };

    [self addHudToView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.HUD showAnimated:YES whileExecutingBlock:^{
        [[RPServerManager sharedManager] getOrderList:dic onSuccess:^(NSMutableArray<RPOrder *> *orderList, NSString *errorMessage) {
            [self hideIndicatorView];
            if (errorMessage==nil) {
                [self fillDriverInfo];
                self.ordersArray = orderList;
                [self.refreshControl endRefreshing];
                [self.tableView reloadData];
            }
            else {
                [self showErrorMessage:errorMessage];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    }];

}

#pragma mark - Navigation
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"orderSegue"]) {
        RPTabBarController *destinationController = [segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        RPOrder *selectedOrder = self.ordersArray[indexPath.row];
        destinationController.orderToken = selectedOrder.token;
    }
}
@end
