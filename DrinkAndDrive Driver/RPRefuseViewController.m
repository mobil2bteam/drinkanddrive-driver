//
//  RPRefuseViewController.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPRefuseViewController.h"
#import "RPRefuseCell.h"
#import "JJMaterialTextfield.h"
#import "Header.h"

@interface RPRefuseViewController()<MBProgressHUDDelegate>
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *refuseTextField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
@implementation RPRefuseViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardDidHideNotification object:nil];
    [self loadRefuses];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}
#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(JJMaterialTextfield *)textField{
    if (textField.text.length==0) {
        [textField showError];
    }else{
        [textField hideError];
    }
}



#pragma mark - Keyboard Notifications
- (void)keyboardWasShown:(NSNotification *)aNotification
{
    NSDictionary *userInfo = [aNotification userInfo];
    CGRect keyboardInfoFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect windowFrame = [self.view.window convertRect:self.view.frame fromView:self.view];
    CGRect keyboardFrame = CGRectIntersection (windowFrame, keyboardInfoFrame);
    CGRect coveredFrame = [self.view.window convertRect:keyboardFrame toView:self.view];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake (0.0, 0.0, coveredFrame.size.height, 0.0);
    [UIView animateWithDuration:0.26 animations:^{
        self.scrollView.contentInset = contentInsets;
        self.scrollView.scrollIndicatorInsets = contentInsets;
    }];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake (0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.26 animations:^{
        self.scrollView.contentInset = contentInsets;
        self.scrollView.scrollIndicatorInsets = contentInsets;
    }];
}

#pragma mark - UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [RPRefuses sharedRefuses].refuses.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RPRefuseCell *cell = (RPRefuseCell*)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.refuseLabel.text = [[RPRefuses sharedRefuses].refuses[indexPath.row] valueForKey:@"name"];
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger refuseId = [[[RPRefuses sharedRefuses].refuses[indexPath.row] valueForKey:@"id"]integerValue];
    NSDictionary *param = @{@"type":@"refuse",
                            @"driverid":@([RPUser sharedUser].driverid),
                            @"password":[RPUser sharedUser].password,
                            @"token":self.token,
                            @"Idrefuse":@(refuseId)
    };
    [self pushRefuseWithParams:param];
}


#pragma mark - Methods
-(void)hideIndicatorView{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}
-(void)addHudToView{
    self.HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.HUD];
    self.HUD.dimBackground = NO;
    self.HUD.delegate = self;
    self.HUD.labelText = @"Загрузка...";
}

-(void)loadRefuses{
    [self addHudToView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.HUD showAnimated:YES whileExecutingBlock:^{
        [[RPServerManager sharedManager] getRefuses:@{@"type":@"getrefuse"} onSuccess:^(NSString *errorMessage) {
            [self hideIndicatorView];
            if (errorMessage==nil) {
                [self.tableView reloadData];
            }
            else {
                [self showErrorMessage:errorMessage];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    }];
}

-(void)pushRefuseWithParams:(NSDictionary*)param{
    [self addHudToView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.HUD showAnimated:YES whileExecutingBlock:^{
        [[RPServerManager sharedManager] pushRefuse:param onSuccess:^(NSString *errorMessage) {
            [self hideIndicatorView];
            if (errorMessage==nil) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            else {
                [self showErrorMessage:errorMessage];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    }];
}
- (IBAction)cancelButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)refuseButtonPressed:(id)sender {
    if (self.refuseTextField.text.length!=0) {
        NSDictionary *param = @{@"type":@"refuse",
                                @"driverid":@([RPUser sharedUser].driverid),
                                @"password":[RPUser sharedUser].password,
                                @"token":self.token,
                                @"comment":self.refuseTextField.text
                                };
        [self pushRefuseWithParams:param];
    } else {
        [self.refuseTextField showError];
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}
@end
