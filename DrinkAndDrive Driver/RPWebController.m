//
//  RPWebController.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPWebController.h"
#import "Header.h"
@interface RPWebController()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation RPWebController
-(void)viewDidLoad{
    [super viewDidLoad];
    NSURL *websiteUrl = [NSURL URLWithString:[RPOptions sharedOptions].user_rates];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:websiteUrl];
    [urlRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [self.webView loadRequest:urlRequest];
    self.navigationController.navigationBar.hidden=NO;

}
@end
