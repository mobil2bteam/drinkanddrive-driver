//
//  RPRouteViewController.h
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/13/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
@interface RPRouteViewController : UIViewController
@property (strong, nonatomic) RPOrderFull *order;
@end
