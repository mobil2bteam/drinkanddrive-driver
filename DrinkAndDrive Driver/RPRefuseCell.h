//
//  RPRefuseCell.h
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPRefuseCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *refuseLabel;

@end
