//
//  RPRouteViewController.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/13/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPRouteViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "RPCloseOrderController.h"
#import "Header.h"

#define kAnimationDuration 0.25
#define klocation  self.appDelegate.locationManager.location.coordinate

@import GoogleMaps;
@interface RPRouteViewController()<GMSMapViewDelegate, RPUpdateLocation,MBProgressHUDDelegate>
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (weak, nonatomic) IBOutlet UIButton *navigationButton;
@property (strong, nonatomic) LMAddress *address;
@property (strong, nonatomic) GMSMarker *marker;
@property (strong, nonatomic) GMSMarker *markerA;
@property (strong, nonatomic) GMSMarker *markerB;
@property (strong, nonatomic) GMSMarker *markerUser;
@property (nonatomic, strong) GMSPolyline *rootRoute;
@property (nonatomic, strong) GMSPolyline *additionalRoute;
@property (assign, nonatomic) BOOL navigationMode;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
//@property (weak, nonatomic) IBOutlet UIButton *arrivedButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchAddressViewTopConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *closeOrderViewTopConstraint;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *streetTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *cityTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *buildingTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *housingTextField;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *homeTextField;

@end

@implementation RPRouteViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.appDelegate = [[UIApplication sharedApplication]delegate];
    self.appDelegate.delegate=self;
    [self prepareMapView];
}
-(void)updateLocationWith:(CLLocationCoordinate2D)coordinate{
    if (self.markerUser==nil) {
        self.markerUser = [[GMSMarker alloc] init];
        self.markerUser.position = coordinate;
        self.markerUser.appearAnimation = kGMSMarkerAnimationPop;
        self.markerUser.icon = [UIImage imageNamed:@"Car"];
        self.markerUser.map=self.mapView;
        [self createPathWithPointA:self.markerUser.position andPointB:self.markerB.position rootRoute:YES];
    } else self.markerUser.position = coordinate;
            if (self.navigationMode) {
                [self.mapView animateToCameraPosition:[GMSCameraPosition
                                                       cameraWithLatitude:self.markerUser.position.latitude
                                                       longitude:self.markerUser.position.longitude
                                                       zoom:self.mapView.camera.zoom]];
    }
}

- (IBAction)searchButtonPressed:(id)sender {
    [self.view endEditing:YES];
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kAnimationDuration animations:^{
        self.searchAddressViewTopConstraint.constant = self.searchAddressViewTopConstraint.constant==0?-210:0;
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)hideSearchView:(id)sender {
    [self.view endEditing:YES];
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kAnimationDuration animations:^{
        self.searchAddressViewTopConstraint.constant=-210;
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)closeOrderButtonPressed:(id)sender {
    [self.view endEditing:YES];
    
    //переключаем статус заявки на статус 99
    NSDictionary *dic = @{@"type":@"setstatus",
                          @"password":[RPUser sharedUser].password,
                          @"driverid":@([RPUser sharedUser].driverid),
                          @"token":self.order.token,
                          @"full_info":@(1),
                          @"status":@(99),
                          @"lan":@(klocation.latitude),
                          @"lng":@(klocation.longitude)
                          };
    [self addHudToView];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.HUD showAnimated:YES whileExecutingBlock:^{
        [[RPServerManager sharedManager]setStatus:dic onSuccess:^(NSString *errorMessage) {
            [self hideIndicatorView];
            if (errorMessage==nil) {
                SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindowWidth:[[UIScreen mainScreen] bounds].size.width * 0.7];
                alert.backgroundType = Blur;
                alert.shouldDismissOnTapOutside = YES;
                [alert showSuccess:self title:nil subTitle:@"Оператор уведомлен" closeButtonTitle:@"OK" duration:0.0f];
            }
            else {
                [self showErrorMessage:errorMessage];
            }
            
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideIndicatorView];
            [self showErrorMessage:@"Произошла ошибка"];
        }];
    }];
}

#pragma mark - GMSMapViewDelegate
- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [self.view endEditing:YES];
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kAnimationDuration animations:^{
        self.searchAddressViewTopConstraint.constant=-210;
        [self.view layoutIfNeeded];
    }];
    
    if (self.marker.map==nil) {
        [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:coordinate                                                  service:kLMGeocoderGoogleService
                                            completionHandler:^(NSArray *results, NSError *error) {
                                                if (results.count && !error) {
                                                    LMAddress *address = [results firstObject];
                                                    if (![NSString isEmpty:[address parseThoroughfare]]) {
                                                        self.address = address;
                                                        self.marker = [[GMSMarker alloc]init];;
                                                        self.marker.position = coordinate;
                                                        self.marker.title = address.parseThoroughfare;
                                                        self.marker.flat=YES;
                                                        self.marker.icon = [UIImage imageNamed:@"PointSearch"];
                                                        self.marker.map = self.mapView;
                                                    }
                                                }
                                            }];
        
        
    }
}

- (void)mapView:(GMSMapView *)mapView
didTapInfoWindowOfMarker:(GMSMarker *)marker{
    if (marker == self.marker) {
        [self.view endEditing:YES];
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *deleteAction = [UIAlertAction
                                       actionWithTitle:@"Удалить"
                                       style:UIAlertActionStyleDestructive
                                       handler:^(UIAlertAction *action)
                                       {
                                           self.additionalRoute.map=nil;
                                           self.marker.map=nil;
                                           self.marker=nil;
                                       }];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"Отменить"
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        
        UIAlertAction *getRoute =   [UIAlertAction
                                     actionWithTitle:@"Проложить маршрут"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                                         if (self.markerUser!=nil) {
                                             [self createPathWithPointA:self.markerUser.position andPointB:marker.position rootRoute:NO];
                                         }
                                     }];
        UIAlertAction *newAddress =  [UIAlertAction
                                    actionWithTitle:@"Конечный адрес"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action)
                                    {
                                        [self addHudToView];
                                        [self.HUD showAnimated:YES whileExecutingBlock:^{
                                            NSMutableDictionary *dic = [@{@"type":@"editpointto",
                                                                  @"token":self.order.token,
                                                                  @"password":[RPUser sharedUser].password,
                                                                  @"driverid":@([RPUser sharedUser].driverid),
                                                                  @"city_to":self.address.locality,
                                                                  @"lan_to":@(self.address.coordinate.latitude),
                                                                  @"lng_to":@(self.address.coordinate.longitude)
                                                                  }mutableCopy];
                                            if (![NSString isEmpty:self.address.street]) {
                                                dic[@"street_to"]=self.address.street;
                                            }
                                            if (![NSString isEmpty:self.address.house]) {
                                                dic[@"house_to"]=self.address.house;
                                            }
                                            if (![NSString isEmpty:self.address.korpus]) {
                                                dic[@"korpus_to"]=self.address.korpus;
                                            }
                                            if (![NSString isEmpty:self.address.stroenie]) {
                                                dic[@"stroenie_to"]=self.address.stroenie;
                                            }
                                            [[RPServerManager sharedManager]editPointTo:dic onSuccess:^(NSString *errorMessage) {
                                                if (errorMessage==nil) {
                                                    if (self.markerB==nil) {
                                                        self.markerB = [[GMSMarker alloc]init];
                                                        self.markerB.icon = [UIImage imageNamed:@"PointBDark"];
                                                        self.markerB.flat=YES;
                                                        self.markerB.map = self.mapView;
                                                    }
                                                    self.markerB.position = self.marker.position;
                                                    self.marker.map=nil;
                                                    self.marker=nil;
                                                    [self redrawRoutesButtonPressed:nil];
                                                } else {
                                                    [self showErrorMessage:errorMessage];
                                                }
                                                
                                            } onFailure:^(NSError *error, NSInteger statusCode) {
                                                [self showErrorMessage:@"Произошла ошибка"];
                                            }];
                                        }];
                                    }];
        [alertController addAction:cancelAction];
        [alertController addAction:newAddress];
        [alertController addAction:getRoute];
        [alertController addAction:deleteAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


-(void)prepareMapView{
    RPLocalAddress *addressA = [self.order.wayList firstObject];
    RPLocalAddress *addressB = [self.order.wayList lastObject];

    self.markerA = [[GMSMarker alloc]init];
    self.markerA.position = addressA.location;
    self.markerA.title = [NSString stringWithFormat:@"%@,%@",addressA.street, addressA.house];
    self.markerA.icon = [UIImage imageNamed:@"PointADark"];
    self.markerA.flat=YES;
    self.markerA.map = self.mapView;
    
    if (addressB.locationTo.latitude!=0 && addressB.locationTo.longitude!=0) {
        self.markerB = [[GMSMarker alloc]init];
        self.markerB.position = addressB.locationTo;
        self.markerB.title = [NSString stringWithFormat:@"%@,%@",addressB.streetTo, addressB.houseTo];
        self.markerB.icon = [UIImage imageNamed:@"PointBDark"];
        self.markerB.flat=YES;
        self.markerB.map = self.mapView;
        [self createPathWithPointA:addressA.location andPointB:addressB.locationTo rootRoute:YES];
    }
    [self animateToMarkerPosition];
}

-(void)animateToMarkerPosition{
    [self.mapView animateToCameraPosition:[GMSCameraPosition
                                           cameraWithLatitude:self.markerA.position.latitude
                                           longitude:self.markerA.position.longitude
                                           zoom:11]];
}

-(void)createPathWithPointA:(CLLocationCoordinate2D)pointA andPointB:(CLLocationCoordinate2D)pointB rootRoute:(BOOL)root{
    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false&units=metric&mode=driving&key=%@",pointA.latitude,pointA.longitude,pointB.latitude,pointB.longitude,[[NSUserDefaults standardUserDefaults]valueForKey:@"serverKey"]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"status"] isEqualToString:@"OK"]) {
            NSString *distance = [[[[responseObject valueForKeyPath:@"routes.legs.distance"]firstObject]valueForKey:@"text"]firstObject];
            NSString *duration = [[[[responseObject valueForKeyPath:@"routes.legs.duration"]firstObject]valueForKey:@"text"]firstObject];
            if (root) {
                self.rootRoute.map=nil;
                self.markerB.snippet = [NSString stringWithFormat:@"Путь: %@ Время: %@",distance,duration];
                if (self.address!=nil) {
                    self.markerB.title = [NSString stringWithFormat:@"%@, %@",self.address.street,self.address.house];
                }
                GMSPath *path =[GMSPath pathFromEncodedPath:responseObject[@"routes"][0][@"overview_polyline"][@"points"]];
                self.rootRoute = [GMSPolyline polylineWithPath:path];
                self.rootRoute.strokeWidth = 8;
                self.rootRoute.strokeColor = [UIColor redColor];
                self.rootRoute.map = self.mapView;
            } else {
                self.additionalRoute.map=nil;
                self.marker.snippet = [NSString stringWithFormat:@"Путь: %@ Время: %@",distance,duration];
                GMSPath *path =[GMSPath pathFromEncodedPath:responseObject[@"routes"][0][@"overview_polyline"][@"points"]];
                self.additionalRoute = [GMSPolyline polylineWithPath:path];
                self.additionalRoute.strokeWidth = 8;
                self.additionalRoute.strokeColor = [UIColor colorWithRed:0.1451 green:0.1647 blue:0.1922 alpha:1.0];
                self.additionalRoute.map = self.mapView;
            }

        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
}

- (IBAction)navigationModeButtonPressed:(id)sender {
    if (self.navigationMode) {
        self.navigationButton.backgroundColor = [UIColor colorWithRed:0.6 green:1.0 blue:0.4 alpha:1.0];
    } else self.navigationButton.backgroundColor = [UIColor colorWithRed:0.9804 green:0.6902 blue:0.1843 alpha:1.0];
    self.navigationMode=!self.navigationMode;
}

- (IBAction)redrawRoutesButtonPressed:(id)sender {
    self.rootRoute.map=nil;
    if (self.markerUser!=nil) {
        if (self.markerB!=nil) {
            [self createPathWithPointA:self.markerUser.position andPointB:self.markerB.position rootRoute:YES];
        }
    } else {
        if (self.markerB!=nil) {
            [self createPathWithPointA:self.markerA.position andPointB:self.markerB.position rootRoute:YES];
        }
    }
    if (self.additionalRoute!=nil) {
        self.additionalRoute.map=nil;
        [self createPathWithPointA:self.markerUser.position andPointB:self.marker.position rootRoute:NO];
    }
    
}

#pragma mark - Methods

- (IBAction)searchAddressButtonPressed:(id)sender {
    if ([NSString isEmpty:self.cityTextField.text]) {
        return;
    }
    [self.view endEditing:YES];
    NSMutableString *sentence = [[NSString stringWithFormat:@"Россия,%@",_cityTextField.text]mutableCopy];
  
    if (![NSString isEmpty:self.streetTextField.text]) {
        sentence = [[sentence stringByAppendingString:[NSString stringWithFormat:@",%@",self.streetTextField.text]]mutableCopy];
    }
    
    if (![NSString isEmpty:self.homeTextField.text]) {
        sentence = [[sentence stringByAppendingString:[NSString stringWithFormat:@",%@",self.homeTextField.text]]mutableCopy];
    }
    
    if (![NSString isEmpty:self.housingTextField.text]) {
        sentence = [[sentence stringByAppendingString:[NSString stringWithFormat:@",%@",self.housingTextField.text]]mutableCopy];
    }
    if (![NSString isEmpty:self.buildingTextField.text]) {
        sentence = [[sentence stringByAppendingString:[NSString stringWithFormat:@",%@",self.buildingTextField.text]]mutableCopy];
    }
    [self.view endEditing:YES];
    
    [[LMGeocoder sharedInstance]geocodeAddressString:sentence service:kLMGeocoderGoogleService completionHandler:^(NSArray *results, NSError *error) {
        if (results.count && !error) {
            [self hideSearchView:self];
            LMAddress *address = [results firstObject];
            self.additionalRoute.map=nil;
            self.marker.map = nil;
            self.marker = nil;
            self.additionalRoute = nil;
            self.address = address;
            self.marker = [[GMSMarker alloc]init];
            self.marker.position = address.coordinate;
            self.marker.flat=YES;
            self.marker.icon = [UIImage imageNamed:@"PointSearch"];
            self.marker.map = self.mapView;
            if (![NSString isEmpty:[address parseThoroughfare]]) {
                self.marker.title = address.parseThoroughfare;
            } else self.marker.title = @"Москва";
            [self.mapView animateToLocation:address.coordinate];
        }
    }];
}

#pragma mark - Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"closeOrderSegue"]) {
        RPCloseOrderController *destinationControleller = segue.destinationViewController;
        destinationControleller.token = self.order.token;
        destinationControleller.userLocation = self.appDelegate.locationManager.location.coordinate;
    }

}

#pragma mark - Methods

-(void)hideIndicatorView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}
-(void)addHudToView{
    self.HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.HUD];
    self.HUD.dimBackground = NO;
    self.HUD.delegate = self;
    self.HUD.labelText = @"Загрузка...";
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(JJMaterialTextfield *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(JJMaterialTextfield *)textField{
    if (textField.text.length==0) {
        [textField showError];
    }else{
        [textField hideError];
    }
}

@end
