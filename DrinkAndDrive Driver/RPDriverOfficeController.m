//
//  RPDriverOfficeController.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPDriverOfficeController.h"
#import "Header.h"

@implementation RPDriverOfficeController
- (IBAction)callOperatorButtonPressed:(id)sender {
    NSArray* words = [[RPOptions sharedOptions].operatorPhone componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phone = [words componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phone]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}
- (IBAction)exitButtonPressed:(id)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"phone"];
    [userDefaults removeObjectForKey:@"pushCoordinate"];
    [userDefaults removeObjectForKey:@"password"];
    [userDefaults synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
