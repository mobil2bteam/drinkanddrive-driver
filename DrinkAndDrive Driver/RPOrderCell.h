//
//  RPOrderCell.h
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPOrderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *hotOrderImageView;
@property (weak, nonatomic) IBOutlet UILabel *dateOrderLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderStatusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pointAImageView;
@property (weak, nonatomic) IBOutlet UIImageView *pointBImageView;
@property (weak, nonatomic) IBOutlet UILabel *addressALabel;
@property (weak, nonatomic) IBOutlet UILabel *addressBLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hotOrderImageViewWidth;

@end
