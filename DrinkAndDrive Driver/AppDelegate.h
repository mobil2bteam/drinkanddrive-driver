//
//  AppDelegate.h
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/9/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/CloudMessaging.h>
#import <CoreLocation/CoreLocation.h>
#import "RPUpdateLocation.h"
#import "LocationTracker.h"

@class LocationTracker;
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property LocationTracker * locationTracker;
@property (nonatomic, strong) CLLocationManager* locationManager;
@property (nonatomic) NSTimer* locationUpdateTimer;
@property (nonatomic, weak) id<RPUpdateLocation> delegate;

@property(nonatomic, readonly, strong) NSString *registrationKey;
@property(nonatomic, readonly, strong) NSString *messageKey;
@property(nonatomic, readonly, strong) NSString *gcmSenderID;
@property(nonatomic, strong) NSString* registrationToken;
@property(nonatomic, readonly, strong) NSDictionary *registrationOptions;

-(void)sendTokenToServer;
@end

