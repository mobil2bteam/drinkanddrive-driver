//
//  RPOrderDescriptionController.h
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPOrderFull;
@interface RPOrderDescriptionController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *addressFromLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressToLabel;
@property (strong, nonatomic) RPOrderFull *order;
@property (strong, nonatomic) NSString *token;
@end
