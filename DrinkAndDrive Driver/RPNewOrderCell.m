//
//  RPNewOrderCell.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPNewOrderCell.h"

@implementation RPNewOrderCell

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:YES];
    if (!highlighted) {
        self.contentView.backgroundColor = [UIColor colorWithRed:0.1451 green:0.1647 blue:0.1922 alpha:1.0];
    } else
    {
        self.contentView.backgroundColor = [UIColor colorWithRed:0.6 green:1.0 blue:0.4 alpha:1.0];
    }
}

@end
