//
//  RPMapController.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPMapController.h"
#import "Header.h"
#import "RPOrderDescriptionController.h"
@interface RPMapController()
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@end

@implementation RPMapController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self prepareMapView];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self createPath];
}
-(void)prepareMapView{
    GMSCameraPosition *camera = [GMSCameraPosition
                                 cameraWithLatitude:55.755826
                                 longitude: 37.6173
                                 zoom:12];
    self.mapView.camera = camera;
}

-(void)createPath{
    RPOrderDescriptionController *orderController = (RPOrderDescriptionController*)(self.tabBarController.viewControllers[0]);
    RPLocalAddress *firstAddress = [orderController.order.wayList firstObject];
    RPLocalAddress *lastAddress = [orderController.order.wayList lastObject];

    GMSMarker *markerA = [[GMSMarker alloc] init];
    markerA.position = firstAddress.location;
    markerA.appearAnimation = kGMSMarkerAnimationPop;
    markerA.icon = [UIImage imageNamed:@"PointADark"];
    markerA.flat=YES;
    if (![NSString isEmpty:orderController.addressFromLabel.text]) {
        markerA.title = orderController.addressFromLabel.text;
    }
    markerA.map = self.mapView;
    if (lastAddress.locationTo.latitude!=0 && lastAddress.locationTo.longitude!=0) {
        GMSMarker *markerB = [[GMSMarker alloc] init];
        markerB.position = lastAddress.locationTo;
        markerB.appearAnimation = kGMSMarkerAnimationPop;
        markerB.icon = [UIImage imageNamed:@"PointBDark"];
        markerB.flat=YES;
        markerB.map = self.mapView;
        if (![NSString isEmpty:orderController.addressToLabel.text]) {
            markerB.title = orderController.addressToLabel.text;
        }
        [self createPathWithPointA:markerA.position andPointB:markerB.position];
    }
    [self.mapView animateToLocation:markerA.position];

}

-(void)createPathWithPointA:(CLLocationCoordinate2D)pointA andPointB:(CLLocationCoordinate2D)pointB{
    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false&units=metric&mode=driving&key=%@",pointA.latitude,pointA.longitude,pointB.latitude,pointB.longitude,[[NSUserDefaults standardUserDefaults]valueForKey:@"serverKey"]];    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"status"] isEqualToString:@"OK"]) {
                GMSPath *path =[GMSPath pathFromEncodedPath:responseObject[@"routes"][0][@"overview_polyline"][@"points"]];
                GMSPolyline *route = [GMSPolyline polylineWithPath:path];
                route.strokeWidth = 8;
                route.strokeColor = [UIColor redColor];
                route.map = self.mapView;
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
}

@end
