//
//  RPUser.h
//  DrinkAndDrive
//
//  Created by Ruslan on 11/27/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface RPUser : NSObject
+ (RPUser*) sharedUser;
@property (nonatomic, strong) NSString *name1;
@property (nonatomic, strong) NSString *name2;
@property (nonatomic, strong) NSString *name3;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *foto;
@property (nonatomic, assign) NSInteger rating;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, strong) NSString *statusText;
@property (nonatomic, assign) NSInteger active;
@property (nonatomic, assign) NSInteger driverid;
@end
