//
//  RPServerManager.h
//  DrinkAndDrive
//
//  Created by Ruslan on 11/23/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"
#import "RPUser.h"
#import "RPOptions.h"
#import "RPAddress.h"
#import "RPOrder.h"
#import "RPOrderFull.h"
#import "RPRefuses.h"
#import "RPTarif.h"

@class RPOrderFull;
@interface RPServerManager : NSObject
+ (RPServerManager*) sharedManager;
- (void) loginWithParameters:(NSDictionary*)param
                               onSuccess:(void(^)(RPUser *user, NSString *errorMessage)) success
                               onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) getConfigsOnSuccess:(void(^)(RPOptions *options)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) getOrderList:(NSDictionary*)param
            onSuccess:(void(^)(NSMutableArray <RPOrder*> *orderList, NSString *errorMessage)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) getOrderFull:(NSDictionary*)param
            onSuccess:(void(^)(RPOrderFull *order, NSString *errorMessage)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) setStatus:(NSDictionary*)param
         onSuccess:(void(^)(NSString *errorMessage)) success
         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) getRefuses:(NSDictionary*)param
          onSuccess:(void(^)(NSString *errorMessage)) success
          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) pushRefuse:(NSDictionary*)param
          onSuccess:(void(^)(NSString *errorMessage)) success
          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) getClosedOrdersList:(NSDictionary*)param
                   onSuccess:(void(^)(NSMutableArray <RPOrder*> *orderList, NSString *errorMessage)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) getTarif:(NSDictionary*)param
        onSuccess:(void(^)(RPTarif *tarif, NSString *errorMessage)) success
        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) editPointTo:(NSDictionary*)param
           onSuccess:(void(^)(NSString *errorMessage)) success
           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) pushCoordinate:(NSDictionary*)param
              onSuccess:(void(^)(NSString *errorMessage)) success
              onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) sendToken:(NSDictionary*)param
         onSuccess:(void(^)(NSString *errorMessage)) success
         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
@end
