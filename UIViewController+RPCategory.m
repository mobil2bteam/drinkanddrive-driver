//
//  UIViewController+RPCategory.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/18/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UIViewController+RPCategory.h"
#import "Reachability.h"
#import "SCLAlertView/SCLAlertView.h"

@implementation UIViewController (RPCategory)

-(BOOL)isReachility{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}

-(void)showErrorMessage:(NSString*)errorMessage{
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindowWidth:[[UIScreen mainScreen] bounds].size.width * 0.7];
    alert.backgroundType = Blur;
    alert.shouldDismissOnTapOutside = YES;
    [alert showError:self title:nil
            subTitle:errorMessage
    closeButtonTitle:@"OK" duration:0.0f];
}

-(void)showWarningMessage:(NSString*)errorMessage{
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindowWidth:[[UIScreen mainScreen] bounds].size.width * 0.7];
    alert.backgroundType = Blur;
    alert.shouldDismissOnTapOutside = YES;
    [alert showWarning:self title:nil
              subTitle:errorMessage
      closeButtonTitle:@"OK" duration:0.0f];
}

-(void)showSuccessMessage:(NSString*)message{
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindowWidth:[[UIScreen mainScreen] bounds].size.width * 0.7];
    alert.backgroundType = Blur;
    alert.shouldDismissOnTapOutside = YES;
    [alert addButton:@"OK" actionBlock:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert showSuccess:self title:nil subTitle:message closeButtonTitle:nil duration:0.0f];
}

-(void)addLogoToNavigationBar{
    if (self.navigationController==nil) {
        return;
    }
    UIView *logoView = [[UIView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2.0-50, 5.5, 100, 33)];
    [logoView setBackgroundColor:[UIColor clearColor]];
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:logoView.bounds];
    logoImageView.image = [UIImage imageNamed:@"Logo"];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    [logoView addSubview:logoImageView];
    [self.navigationController.navigationBar addSubview:logoView];
}

@end
