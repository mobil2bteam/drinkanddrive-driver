//
//  RPOrder.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/17/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPLocalAddress.h"
@interface RPOrder : NSObject
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *adddressA;
@property (nonatomic, strong) NSString *adddressB;
@property (nonatomic, assign) NSInteger numberOrder;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, assign) NSInteger rating;
@property (nonatomic, assign) NSInteger orderId;
@property (nonatomic, assign) NSInteger flagTime;
@property (nonatomic, strong) NSString *statusText;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, assign) NSInteger itog;
@property (nonatomic, assign) BOOL isCurrent;
+ (RPOrder*) sharedOrder;
@end
