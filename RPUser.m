//
//  RPUser.m
//  DrinkAndDrive
//
//  Created by Ruslan on 11/27/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import "RPUser.h"

@implementation RPUser
+ (RPUser*) sharedUser {
    static RPUser* user = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        user = [[RPUser alloc] init];
    });
    return user;
}
@end
