//
//  RPTabBarController.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPTabBarController.h"

@implementation RPTabBarController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self.tabBar setTintColor:[UIColor colorWithRed:0.6 green:1.0 blue:0.4 alpha:1.0]];
}
@end
