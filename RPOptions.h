//
//  RPOptions.h
//  DrinkAndDrive
//
//  Created by Ruslan on 11/27/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPOptions : NSObject
+ (RPOptions*) sharedOptions;
@property (nonatomic, strong) NSString *user_agreement;
@property (nonatomic, strong) NSString *user_rates;
@property (nonatomic, strong) NSString *operatorPhone;
@property (nonatomic, strong) NSString *about_phone;
@property (nonatomic, strong) NSString *about_suite;
@property (nonatomic, strong) NSString *url_suite;
@property (nonatomic, assign) NSInteger count_ball;
@property (nonatomic, strong) NSMutableArray *typecar;
@end
