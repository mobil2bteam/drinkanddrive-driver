//
//  RPTabBarController.h
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPTabBarController : UITabBarController
@property (strong, nonatomic) NSString *orderToken;
// заявка закрыта или нет (есть 2 режима просмотра - заявки в реальном времени и заявки с архива)
@property (nonatomic) BOOL isClosedOrder;
@end
