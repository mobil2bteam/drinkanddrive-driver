//
//  RPOrderFull.h
//  DrinkAndDrive
//
//  Created by Ruslan on 12/27/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"
@import CoreLocation;
@interface RPOrderFull : NSObject
@property (nonatomic, assign) NSInteger orderId;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *dateCreate;
@property (nonatomic, strong) NSString *dateStart;
@property (nonatomic, strong) NSString *datePlane;
@property (nonatomic, strong) NSString *dateFinish;
@property (nonatomic, strong) NSString *dateGo;

@property (nonatomic, strong) NSString *marka;
@property (nonatomic, strong) NSString *numberCar;
@property (nonatomic, strong) NSString *userPhone;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger driverId;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, strong) NSString *statusText;

@property (nonatomic, assign) NSInteger payBall;
@property (nonatomic, assign) CLLocationCoordinate2D userCoordinate;
@property (nonatomic, assign) NSInteger flagTime;
@property (nonatomic, assign) NSInteger flagReg;
@property (nonatomic, assign) NSInteger itog;
@property (nonatomic, assign) NSInteger summ;
@property (nonatomic, assign) NSInteger summBall;
@property (nonatomic, assign) NSInteger addBall;
@property (nonatomic, strong) NSString *comment;
@property (nonatomic, strong) NSString *currentTime;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *mesageForClient;
@property (nonatomic, strong) NSString *adddressA;
@property (nonatomic, strong) NSString *adddressB;
@property (nonatomic, assign) NSInteger ocenka;
@property (nonatomic, assign) NSInteger numberOrder;


@property (nonatomic, strong) NSMutableArray <RPLocalAddress *> *wayList;

@end
