//
//  RPRefuses.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPRefuses.h"

@implementation RPRefuses
+ (RPRefuses*) sharedRefuses{
    static RPRefuses* refuses = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        refuses = [[RPRefuses alloc] init];
    });
    return refuses;
}
@end
