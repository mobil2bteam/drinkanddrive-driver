//
//  RPServerManager.m
//  DrinkAndDrive
//
//  Created by Ruslan on 11/23/15.
//  Copyright © 2015 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "RPServerManager.h"
#import "AFNetworking.h"
#import "RPUser.h"
#import "RPOptions.h"
#import "RPAddress.h"
#import "RPOrder.h"
#import "RPTarif.h"
#import "LocationTracker.h"
#define kAddressSite @"http://www.dndmobil.ru/"

@interface RPServerManager ()
@property (strong, nonatomic) AFHTTPSessionManager *requestOperationManager;
@end


@implementation RPServerManager : NSObject


+ (RPServerManager*) sharedManager {
    static RPServerManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[RPServerManager alloc] init];
    });
    return manager;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

-(void)initWithUrl:(NSURL*)url{
    self.requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    self.requestOperationManager.responseSerializer= [AFJSONResponseSerializer serializer];
    self.requestOperationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
}


- (void) loginWithParameters:(NSDictionary*)param
                               onSuccess:(void(^)(RPUser *user, NSString *errorMessage)) success
                               onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@driver.php?",kAddressSite]];
    [self initWithUrl:url];
    __block NSString *errorMessage=nil;
     [self.requestOperationManager POST:@"" parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
        if ([data valueForKeyPath:@"message.msg"]!=nil) {
                         errorMessage = [data valueForKeyPath:@"message.msg"];
                     } else if ([[data valueForKeyPath:@"driver_info.active"]integerValue]==0) {
                         errorMessage = @"Произошла ошибка авторизации";
                     } else {
                         NSDictionary *userInfo = [data valueForKey:@"driver_info"];
                         [self updateDriverInforFrom:userInfo];
                     }
                     if (success) {
                         success(nil,errorMessage);
                     }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure)
                     {
                         failure(error,404);
                     }
    }];
}

- (void) getConfigsOnSuccess:(void(^)(RPOptions *options)) success
onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@mobindex.php?",kAddressSite]];
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:@{@"type":@"getoptions"}
                              progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
        [RPOptions sharedOptions].operatorPhone = [data valueForKey:@"operator"];
        [RPOptions sharedOptions].user_rates = [data valueForKey:@"user_rates"];
        [RPOptions sharedOptions].user_agreement = [data valueForKey:@"user_agreement"];
        [RPOptions sharedOptions].about_phone = [data valueForKey:@"about_phone"];
        [RPOptions sharedOptions].about_suite = [data valueForKey:@"about_suite"];
        [RPOptions sharedOptions].url_suite = [data valueForKey:@"url_suite"];
        [RPOptions sharedOptions].count_ball = [[data valueForKey:@"count_ball"]integerValue];
        [RPOptions sharedOptions].typecar = [[NSMutableArray alloc]init];
        for (NSDictionary *dic in [data valueForKey:@"typecar"]) {
            NSInteger typeId = [[dic valueForKey:@"id"]integerValue];
            NSString *name = [dic valueForKey:@"name"];
            NSDictionary *car = @{@"id":@(typeId),
                                  @"name":name};
            [[RPOptions sharedOptions].typecar addObject:car];
        }
        if (success) {
            success(nil);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,404);
        }
    }];
}


- (void) getOrderList:(NSDictionary*)param
            onSuccess:(void(^)(NSMutableArray <RPOrder*> *orderList, NSString *errorMessage)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@driver.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    __block NSMutableArray <RPOrder*> *orderList = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  } else {
                                      [self updateDriverInforFrom:data[@"driver_info"]];
                                      orderList = [[NSMutableArray <RPOrder*> alloc]init];
                                      if ([data valueForKey:@"current"]!=nil) {
                                          //
                                              [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"pushCoordinate"];
                                              [[NSUserDefaults standardUserDefaults]synchronize];
                                          //
                                          NSDictionary *dic = [data valueForKey:@"current"];
                                          RPOrder *order = [[RPOrder alloc]init];
                                          order.isCurrent=YES;
                                          order.flagTime = [dic[@"flag_time"]integerValue];
                                          order.numberOrder = [dic[@"number_order"]integerValue];
                                          order.orderId = [dic[@"id"]integerValue];
                                          order.status = [dic[@"status"]integerValue];
                                          order.statusText = dic[@"status_txt"];
                                          order.adddressA = dic[@"address_from"];
                                          order.adddressB = dic[@"address_to"];
                                          order.token = dic[@"token"];
                                          order.date = dic[@"date_plane"];
                                          [orderList addObject:order];
                                      } else {
//                                          [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"pushCoordinate"];
//                                          [[NSUserDefaults standardUserDefaults]synchronize];
                                      }
                                      NSArray *array = [data valueForKey:@"orders"];
                                      for (NSDictionary *dic in array) {
                                          RPOrder *order = [[RPOrder alloc]init];
                                          order.isCurrent=NO;
                                          order.flagTime = [dic[@"flag_time"]integerValue];
                                          order.numberOrder = [dic[@"number_order"]integerValue];
                                          order.orderId = [dic[@"id"]integerValue];
                                          order.status = [dic[@"status"]integerValue];
                                          order.statusText = dic[@"status_txt"];
                                          order.adddressA = dic[@"address_from"];
                                          order.adddressB = dic[@"address_to"];
                                          order.token = dic[@"token"];
                                          order.date = dic[@"date_plane"];
                                          [orderList addObject:order];
                                      }
                                  }
                                  if (success) {
                                      success(orderList,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}


- (void) getOrderFull:(NSDictionary*)param
            onSuccess:(void(^)(RPOrderFull *order, NSString *errorMessage)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    __block RPOrderFull *order = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  } else {
                                      order = [[RPOrderFull alloc]init];
                                      NSDictionary *dic = [data valueForKey:@"order_info"];
                                      order.numberOrder = [dic[@"number_order"]integerValue];
                                      order.datePlane = dic[@"date_plane"];
                                      order.dateStart = dic[@"date_start"];
                                      order.adddressA = dic[@"address_from"];
                                      order.adddressB = dic[@"address_to"];
                                      order.dateFinish = dic[@"date_finish"];
                                      order.dateCreate = dic[@"date_create"];
                                      order.currentTime = dic[@"current_time"];
                                      order.time = dic[@"time"];
                                      order.summ = [dic[@"summ"]integerValue];
                                      order.summBall = [dic[@"summ_bal"]integerValue];
                                      order.addBall = [dic[@"add_bal"]integerValue];
                                      order.itog = [dic[@"itog"]integerValue];
                                      order.marka = dic[@"marka"];
                                      order.numberCar = dic[@"number_car"];
                                      order.comment = dic[@"comment"];
                                      order.userPhone = dic[@"user_phone"];
                                      order.userName = dic[@"user_name"];
                                      order.ocenka = [dic[@"ocenka"]integerValue];
                                      order.status = [dic[@"status"]integerValue];
                                      if (order.status>=3) {
                                          [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"pushCoordinate"];
                                          [[NSUserDefaults standardUserDefaults]synchronize];
                                      }
                                      order.token = dic[@"token"];
                                      order.wayList = [[NSMutableArray <RPLocalAddress*> alloc]init];
                                      NSArray *array = [data valueForKey:@"way_list"];
                                      for (NSDictionary *dic in array) {
                                          RPLocalAddress *address = [[RPLocalAddress alloc]init];
                                          address.addressId = [dic[@"id"]integerValue];
                                          address.orderId = [dic[@"orders_id"]integerValue];
                                          address.dateAdd = dic[@"date_add"];
                                          address.location = CLLocationCoordinate2DMake([dic[@"lan_from"]floatValue], [dic[@"lng_from"]floatValue]);
                                          address.city = dic[@"city_from"];
                                          address.street = dic[@"street_from"];
                                          address.house = dic[@"house_from"];
                                          address.korpus = dic[@"korpus_from"];
                                          address.stroenie = dic[@"stroenie_from"];

                                          address.locationTo = CLLocationCoordinate2DMake([dic[@"lan_to"]floatValue], [dic[@"lng_to"]floatValue]);
                                          address.cityTo = dic[@"city_to"];
                                          address.streetTo = dic[@"street_to"];
                                          address.houseTo = dic[@"house_to"];
                                          address.korpusTo = dic[@"korpus_to"];
                                          address.stroenieTo = dic[@"stroenie_to"];
                                          [order.wayList addObject:address];
                                      }
                                      
                                  }
                                  if (success) {
                                      success(order,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
    
}

- (void) setStatus:(NSDictionary*)param
        onSuccess:(void(^)(NSString *errorMessage)) success
        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];

}

- (void) editPointTo:(NSDictionary*)param
         onSuccess:(void(^)(NSString *errorMessage)) success
         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}


- (void) getRefuses:(NSDictionary*)param
         onSuccess:(void(^)(NSString *errorMessage)) success
         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@driver.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  } else {
                                      NSMutableArray *refuses = [[NSMutableArray alloc]init];
                                      
                                      for (NSDictionary *dic in data[@"refuse_list"]){
                                          NSDictionary *refuse = @{@"id":@([dic[@"id"]integerValue]),
                                                                   @"name":dic[@"name"]};
                                          [refuses addObject:refuse];
                                      }
                                      [RPRefuses sharedRefuses].refuses = [refuses copy];
                                  }
                                  
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) pushRefuse:(NSDictionary*)param
          onSuccess:(void(^)(NSString *errorMessage)) success
          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@driver.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) pushCoordinate:(NSDictionary*)param
          onSuccess:(void(^)(NSString *errorMessage)) success
          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@driver.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

- (void) getClosedOrdersList:(NSDictionary*)param
            onSuccess:(void(^)(NSMutableArray <RPOrder*> *orderList, NSString *errorMessage)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@driver.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    __block NSMutableArray <RPOrder*> *orderList = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  } else {
                                      orderList = [[NSMutableArray <RPOrder*> alloc]init];
                                      NSArray *array = [data valueForKey:@"order_list"];
                                      for (NSDictionary *dic in array) {
                                          RPOrder *order = [[RPOrder alloc]init];
                                          order.numberOrder = [dic[@"number_order"]integerValue];
                                          order.rating = [dic[@"rating"]integerValue];
                                          order.adddressA = dic[@"address_b"];
                                          order.adddressB = dic[@"address_a"];
                                          order.token = dic[@"token"];
                                          order.date = dic[@"date_plane"];
                                          order.itog = [dic[@"itog"]integerValue];
                                          [orderList addObject:order];
                                      }
                                  }
                                  if (success) {
                                      success(orderList,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void) getTarif:(NSDictionary*)param
                   onSuccess:(void(^)(RPTarif *tarif, NSString *errorMessage)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@order.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    __block RPTarif *tarif = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  } else {
                                      tarif =[[RPTarif alloc]init];
                                      NSDictionary *dic = data[@"check_info"];
                                      tarif.name = dic[@"tarif"][@"name"];
                                      tarif.summFirstHour = [dic[@"tarif"][@"summ_first_hour"]integerValue];
                                      tarif.summNextMinute = [dic[@"tarif"][@"summ_next_minut"]integerValue];
                                      tarif.summNextKm = [dic[@"tarif"][@"summ_next_km"]integerValue];
                                      tarif.time = [dic[@"time"]integerValue];
                                      tarif.distanceFromMkad = [dic[@"distance_from_mkad"]integerValue];
                                      tarif.summForTime = [dic[@"summ_for_time"]integerValue];
                                      tarif.summForDistance = [dic[@"summ_for_distance"]integerValue];
                                      tarif.summForTarif = [dic[@"summ_for_tarif"]integerValue];
                                      tarif.summPayBal = [dic[@"summ_pay_ball"]integerValue];
                                      tarif.summForItog = [dic[@"summ_for_tarif"]integerValue];
                                      tarif.addBal = [dic[@"add_ball"]integerValue];
                                  }
                                  if (success) {
                                      success(tarif,errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}


-(void)updateDriverInforFrom:(NSDictionary *)userInfo{
    [RPUser sharedUser].active = [[userInfo valueForKey:@"active"]integerValue];
    [RPUser sharedUser].phone = [userInfo valueForKey:@"phone"];
    [RPUser sharedUser].name1 = [userInfo valueForKey:@"name1"];
    [RPUser sharedUser].name2 = [userInfo valueForKey:@"name2"];
    [RPUser sharedUser].name3 = [userInfo valueForKey:@"name3"];
    [RPUser sharedUser].phone = [userInfo valueForKey:@"phone"];
    [RPUser sharedUser].foto = [userInfo valueForKey:@"foto"];
    [RPUser sharedUser].name3 = [userInfo valueForKey:@"name3"];
    [RPUser sharedUser].rating = [[userInfo valueForKey:@"rating"]integerValue];
    [RPUser sharedUser].statusText = [userInfo valueForKey:@"status_txt"];
    [RPUser sharedUser].status = [[userInfo valueForKey:@"status"]integerValue];
    [RPUser sharedUser].password = [userInfo valueForKey:@"password"];
    [RPUser sharedUser].driverid = [[userInfo valueForKey:@"id"]integerValue];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[RPUser sharedUser].password forKey:@"password"];
    [userDefaults setInteger:1 forKey:@"pushCoordinate"];
    [userDefaults synchronize];
}

- (void) sendToken:(NSDictionary*)param
         onSuccess:(void(^)(NSString *errorMessage)) success
         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@driver.php?",kAddressSite]];
    __block NSString *errorMessage = nil;
    [self initWithUrl:url];
    [self.requestOperationManager POST:@"" parameters:param
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  }
                                  
                                  if (success) {
                                      success(errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}
@end
